class CreateSpaces < ActiveRecord::Migration
  def self.up
    create_table :spaces do |t|
      t.integer :organization_id,   default: 0, null: false
      t.string :name,               default: "", limit: 250
      t.string :description,        default: "", limit: 2000
      t.boolean :status,            default: true, null: false

      t.timestamps
    end

    add_index :spaces, :name
  end

  def self.down
    remove_index :spaces, :name

    drop_table :spaces
  end
end
