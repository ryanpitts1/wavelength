module WikiHelper
  # Returns the full title on a per-page basis.
  def wiki_full_title(page_title)
    base_title = "Wiki | Wavelength"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}".html_safe
    end
  end

  def display_pages_in_space
    space = params[:space_id] ? params[:space_id] : params[:id]
    if space
      @root_pages_for_current_space = @space.pages.where(ancestry: nil).order("sort_order ASC, id ASC")
      @pages_for_current_space = []
      @root_pages_for_current_space.each do |rp|
        @pages_for_current_space << rp.subtree.arrange(order: "sort_order ASC, name ASC")
      end
      build_pages_navigation @pages_for_current_space
    end
  end

  def build_pages_navigation(root_pages)
    root_pages.map do |pages|
      nested_pages pages
    end.join.html_safe
  end

  def nested_pages(pages)
    pages.map do |page, sub_pages|
      render(page) + (content_tag(:ul, nested_pages(sub_pages), class: "nested-pages list-unstyled") unless sub_pages.blank?)
    end.join.html_safe
  end

  def display_page_comments
    page = params[:id]
    if page
      @root_comments_for_current_page = @page.page_comments.where(ancestry: nil).order("id DESC")
      @comments_for_current_page = []
      @root_comments_for_current_page.each do |rc|
        @comments_for_current_page << rc.subtree.arrange(order: "id DESC")
      end
      build_comment_structure @comments_for_current_page
    end
  end

  def build_comment_structure(root_comments)
    root_comments.map do |comments|
      nested_comments comments
    end.join.html_safe
  end

  def nested_comments(comments)
    comments.map do |comment, sub_comments|
      render(comment) + (content_tag(:ul, nested_comments(sub_comments), class: "nested-comments list-unstyled") unless sub_comments.blank?)
    end.join.html_safe
  end
end