Fabricator(:page_comment) do
  page { Fabricate(:page) }
  user { Fabricate(:user) }
  parent_id { "" }
  content { Faker::Lorem.paragraph }
end