require "rails_helper"

feature "User updates account" do
  let(:admin_user) { Fabricate(:admin_user) }

  before do
    admin_user.confirmed_at = Time.now
    admin_user.save
    login_as(admin_user, scope: :user)
    visit "/users/edit"
  end

  scenario "User sees a success message" do

    fill_in "Display Name", with: "Test User"
    click_button "Update Account"
    admin_user.reload

    expect(page).to have_text("Your account has been updated!")
    expect(admin_user.display_name).to eq("Test User")
  end

  scenario "Organization and access code fields are unaffected if leaft blank" do

    find("#user_organization_attributes_name").set("")
    find("#user_organization_attributes_access_code").set("")
    click_button "Update Account"
    admin_user.reload

    expect(page).to have_text("Your account has been updated!")
    expect(admin_user.organization.name).to_not be_blank
  end

  scenario "Admin user updates organization name" do

    find("#user_organization_attributes_name").set("Wavelength")
    click_button "Update Account"
    admin_user.reload

    expect(page).to have_text("Your account has been updated!")
    expect(admin_user.organization.name).to eq("Wavelength")
  end

  scenario "User receives an error if account update is unsuccessful" do

    find("#user_email").set("")
    click_button "Update Account"

    expect(page).to have_text("Something went wrong!")
  end
end