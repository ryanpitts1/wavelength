Fabricator(:organization) do
  name { Faker::Company.name }
  website { Faker::Internet.url }
  description { Faker::Lorem.paragraph }
  access_code { Faker::Internet.password(10, 20) }
end