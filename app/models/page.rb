class Page < ActiveRecord::Base
  has_ancestry

  belongs_to :space
  belongs_to :user
  has_many :page_attachments, dependent: :destroy
  has_many :page_comments

  validates_presence_of :name

  accepts_nested_attributes_for :user, :page_attachments, :page_comments, allow_destroy: true
end
