class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable, :timeoutable

  belongs_to :organization
  has_many :pages
  has_many :page_comments, dependent: :destroy

  accepts_nested_attributes_for :organization

  validates_presence_of :display_name
end
