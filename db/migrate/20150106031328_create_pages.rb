class CreatePages < ActiveRecord::Migration
  def self.up
    create_table :pages do |t|
      t.integer :user_id,           default: 0, null: false
      t.integer :space_id,          default: 0, null: false
      t.string :ancestry,           default: "", limit: 500
      t.string :name,               default: "", limit: 250
      t.string :description,        default: "", limit: 2000
      t.string :content,            default: "", limit: 9000
      t.integer :sort_order,        default: 0
      t.boolean :status,            default: true

      t.timestamps
    end

    add_index :pages, :name
    add_index :pages, :ancestry
  end

  def self.down
    remove_index :pages, :name
    remove_index :pages, :ancestry

    drop_table :pages
  end
end