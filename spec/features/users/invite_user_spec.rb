require "rails_helper"

feature "Admin invites user" do
  let(:admin_user) { Fabricate(:admin_user) }

  before do
    admin_user.confirmed_at = Time.now
    admin_user.save
    login_as(admin_user, scope: :user)
    visit "/users/edit"
  end

  scenario "Admin user can invite another user" do
    email = "test@example.com"
    find("#email").set(email)
    click_button "Send Invite"

    expect(page).to have_content("You have invited #{email} to join your organization!")
  end
end