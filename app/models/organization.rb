class Organization < ActiveRecord::Base
  has_many :users, dependent: :destroy
  has_many :spaces, dependent: :destroy

  validates_presence_of :name
end
