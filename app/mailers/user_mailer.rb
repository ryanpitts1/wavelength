class UserMailer < Devise::Mailer   
  helper :application
  include Devise::Controllers::UrlHelpers

  def invite_user(current_user, email, organization, access_code)
    @email = email
    @organization = organization
    @access_code = access_code
    mail(from: "#{current_user.display_name} <#{current_user.email}>", to: "#{email}", subject: "You have been invited to join #{organization} on Wavelength!")
  end
end