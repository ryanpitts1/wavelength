Fabricator(:page) do
  user { Fabricate(:user) }
  space { Fabricate(:space) }
  parent_id { "" }
  name { Faker::Lorem.words(3).join(" ") }
  description { Faker::Lorem.paragraph }
  content { Faker::Lorem.paragraph }
  sort_order { 0 }
  status { true }
end