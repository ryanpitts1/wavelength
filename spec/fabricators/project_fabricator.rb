Fabricator(:project) do
  organization { Fabricate(:organization) }
  point_scale { "fibonacci" }
  name { Faker::Lorem.words(3).join(" ") }
  description { Faker::Lorem.paragraph }
  status { true }
end