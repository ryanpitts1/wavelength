class CreateProjects < ActiveRecord::Migration
  def self.up
    create_table :projects do |t|
      t.integer :organization_id,   null: false
      t.string :point_scale,        default: '', limit: 250
      t.string :name,               default: '', limit: 250
      t.string :description,        default: '', limit: 2000

      t.timestamps
    end

    add_index :projects, :name
  end

  def self.down
    remove_index :projects, :name

    drop_table :projects
  end
end