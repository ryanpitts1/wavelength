class Wiki::StaticPagesController < WikiController
  before_action :authenticate_user!

  def dashboard
    @spaces = Space.paginate(page: params[:page], per_page: 20).where("organization_id = ? AND status = ?", current_user.organization.id, true).order("id DESC")

    @pages = []
    Space.where("organization_id = ?", current_user.organization.id).each do |s|
      s.pages.each { |p| @pages << p }
    end
    @pages.sort_by { |p| p[:updated_at] }.reverse
  end
end