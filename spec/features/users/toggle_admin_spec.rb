require "rails_helper"

feature "Toggle admin access" do
  let(:admin_user) { Fabricate(:admin_user) }
  let(:admin_user_two) { Fabricate(:admin_user) }
  let(:user) { Fabricate(:user) }

  before do
    admin_user.confirmed_at = Time.now
    admin_user.save

    admin_user_two.confirmed_at = Time.now
    admin_user_two.organization_id = admin_user.organization.id
    admin_user_two.save

    user.confirmed_at = Time.now
    user.organization_id = admin_user.organization.id
    user.save

    login_as(admin_user, scope: :user)
    visit "/users/edit"
  end

  scenario "Admin user can make another user an admin" do
    click_on "Grant Admin Access"
    user.reload

    expect(page).to have_text("#{user.display_name} has been granted admin access.")
    expect(user.is_admin).to be true
  end

  scenario "Admin user can revoke admin access of another user" do
    click_on "Revoke Admin Access"
    admin_user_two.reload

    expect(page).to have_text("#{admin_user_two.display_name} has had their admin access revoked.")
    expect(admin_user_two.is_admin).to be false
  end
end