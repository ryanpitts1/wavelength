# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150226223313) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "organizations", force: true do |t|
    t.string   "name",        limit: 250,  default: ""
    t.string   "website",     limit: 500,  default: ""
    t.string   "description", limit: 2000, default: ""
    t.string   "access_code", limit: 500,  default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "organizations", ["name"], name: "index_organizations_on_name", using: :btree

  create_table "page_attachments", force: true do |t|
    t.integer  "page_id",                             null: false
    t.string   "name",       limit: 500, default: ""
    t.string   "file",       limit: 500, default: ""
    t.string   "aws_path",   limit: 500, default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "page_attachments", ["file"], name: "index_page_attachments_on_file", using: :btree

  create_table "page_comments", force: true do |t|
    t.integer  "page_id",                              null: false
    t.integer  "user_id",                              null: false
    t.string   "ancestry",   limit: 500,  default: ""
    t.string   "content",    limit: 9000, default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "page_comments", ["ancestry"], name: "index_page_comments_on_ancestry", using: :btree

  create_table "pages", force: true do |t|
    t.integer  "user_id",                  default: 0,    null: false
    t.integer  "space_id",                 default: 0,    null: false
    t.string   "ancestry",    limit: 500,  default: ""
    t.string   "name",        limit: 250,  default: ""
    t.string   "description", limit: 2000, default: ""
    t.string   "content",     limit: 9000, default: ""
    t.integer  "sort_order",               default: 0
    t.boolean  "status",                   default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pages", ["ancestry"], name: "index_pages_on_ancestry", using: :btree
  add_index "pages", ["name"], name: "index_pages_on_name", using: :btree

  create_table "projects", force: true do |t|
    t.integer  "organization_id",                           null: false
    t.string   "point_scale",     limit: 250,  default: ""
    t.string   "name",            limit: 250,  default: ""
    t.string   "description",     limit: 2000, default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "projects", ["name"], name: "index_projects_on_name", using: :btree

  create_table "spaces", force: true do |t|
    t.integer  "organization_id",              default: 0,    null: false
    t.string   "name",            limit: 250,  default: ""
    t.string   "description",     limit: 2000, default: ""
    t.boolean  "status",                       default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "spaces", ["name"], name: "index_spaces_on_name", using: :btree

  create_table "users", force: true do |t|
    t.integer  "organization_id",        default: 0,     null: false
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "display_name",           default: "",    null: false
    t.boolean  "is_admin",               default: false, null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

end
