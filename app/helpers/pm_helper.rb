module PmHelper
  # Returns the full title on a per-page basis.
  def pm_full_title(page_title)
    base_title = "Spectrum | Wavelength"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}".html_safe
    end
  end
end