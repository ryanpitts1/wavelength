Fabricator(:user) do
  organization { Fabricate(:organization) }
  email { Faker::Internet.email }
  password { "password" }
  display_name { Faker::Name.name }
  is_admin { false }
end

Fabricator(:admin_user, from: :user) do
  is_admin { true }
end