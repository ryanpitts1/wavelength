$(function(){
    $(".comment-reply-link").on("click", function() {
        $(this).next(".comment-form").toggle();
    });

    $(".comment .comment-actions .glyphicon-pencil").on("click", function() {
        $(this).closest(".comment").find(".comment-edit-form").toggle();
        $(this).closest(".comment").find(".comment-content").toggle();
        $(this).toggleClass("glyphicon-pencil glyphicon-remove");
    });
});