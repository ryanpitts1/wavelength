class StaticPagesController < ApplicationController
  before_action :authenticate_user!, only: :dashboard

  def home
    # this is the home page
  end

  def dashboard
    # this is the dashboard page
  end
end