require "rails_helper"

feature "User sign in" do
  let(:user) { Fabricate(:user) }
 
  before do
    user.confirmed_at = Time.now
    user.save
    visit "/sign-in"
  end

  scenario "User successfully signs in" do
    fill_in "Email",         with: user.email
    fill_in "Password",      with: user.password
    click_button "Sign In"

    expect(page).to have_text("Signed in successfully.")
  end

  scenario "User has email/password mismatch" do
    fill_in "Email",         with: user.email
    fill_in "Password",      with: "#{user.password}abc123"
    click_button "Sign In"

    expect(page).to have_text("Invalid email or password.")
  end
end