class Pm::StaticPagesController < PmController
  before_action :authenticate_user!

  def dashboard
    # this is the project management dashboard
  end
end