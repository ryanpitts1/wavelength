class CreatePageComments < ActiveRecord::Migration
  def self.up
    create_table :page_comments do |t|
      t.integer :page_id,           null: false
      t.integer :user_id,           null: false
      t.string :ancestry,           default: "", limit: 500
      t.string :content,            default: "", limit: 9000

      t.timestamps
    end

    add_index :page_comments, :ancestry
  end

  def self.down
    remove_index :page_comments, :ancestry

    drop_table :page_comments
  end
end