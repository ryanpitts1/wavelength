require "rails_helper"

feature "User sign up" do
  let(:organization) { Fabricate(:organization) }

  before { visit "/sign-up" }

  scenario "User creates an account" do
    fill_in "Email",                 with: "test@example.com"
    fill_in "Display Name",          with: "Test User"
    fill_in "Password",              with: "password"
    fill_in "Confirm Password",      with: "password"
    fill_in "Organization Name",     with: "Wavelength"
    click_button "Create Account"

    expect(page).to have_text("Your account has been successfully created!")
  end

  scenario "User receives an error if account creation was unsuccessful" do
    fill_in "Display Name", with: "Test User"
    click_button "Create Account"

    expect(page).to have_text("Something went wrong!")
  end

  scenario "User creates an account and successfully joins an existing organization" do
    fill_in "Email",                      with: "test@example.com"
    fill_in "Display Name",               with: "Test User"
    fill_in "Password",                   with: "password"
    fill_in "Confirm Password",           with: "password"
    fill_in "Organization Name",          with: organization.name
    fill_in "Organization Access Code",   with: organization.access_code
    click_button "Create Account"

    expect(page).to have_text("Your account has been successfully created!")
    expect(organization.users.count).to eq(1)
  end

  scenario "User creates an account and unsuccessfully joins an existing organization" do
    fill_in "Email",                      with: "test@example.com"
    fill_in "Display Name",               with: "Test User"
    fill_in "Password",                   with: "password"
    fill_in "Confirm Password",           with: "password"
    fill_in "Organization Name",          with: organization.name
    fill_in "Organization Access Code",   with: "#{organization.access_code}abc123"
    click_button "Create Account"

    expect(page).to have_text("No organization has been found with that name and access code.")
    expect(organization.users.count).to eq(0)
  end
end