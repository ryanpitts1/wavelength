require "rails_helper"

describe Space do
  let(:space) { Fabricate(:space) }

  it "has valid factory" do
    expect(space).to be_valid
  end

  it "is invalid when given no name" do
    space.name = nil

    expect(space).to_not be_valid
    expect(space.errors[:name]).to include("can't be blank")
  end
end