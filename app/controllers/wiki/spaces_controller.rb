class Wiki::SpacesController < WikiController
  before_action :authenticate_user!

  def show
    @space = Space.find(params[:id])
  end

  def new
    @space = Space.new
  end

  def create
    @space = Space.new(space_params)
    @space.organization_id = current_user.organization_id
    if @space.save
      flash[:success] = "#{@space.name} has been created!"
      redirect_to wiki_space_path(@space)
    else
      flash.now[:error] = "Something went wrong! Please try again."
      render :new
    end
  end

  def edit
    @space = Space.find(params[:id])
  end

  def update
    @space = Space.find(params[:id])
    if @space.update(space_params)
      flash[:notice] = "#{@space.name} has been updated!"
      redirect_to wiki_space_path(@space)
    else
      flash.now[:error] = "Something went wrong! Please try again."
      render :edit
    end
  end

  def destroy
    @space = Space.find(params[:id])
    @space.destroy
    flash[:warning] = "#{@space.name} has been deleted."
    redirect_to wiki_root_path
  end

  def archive
    @space = Space.find(params[:space_id])
    @space.status = false
    if @space.save
      flash[:success] = "#{@space.name} has been archived!"
      redirect_to wiki_root_path
    else
      flash[:error] = "Something went wrong! Please try again."
      redirect_to :back
    end
  end

  private
    def space_params
      params.require(:space).permit(:organization_id, :name, :description, :status, :_destroy, {pages_attributes: [:id, :user_id, :space_id, :parent_id, :name, :description, :content, :sort_oder, :status, :_destroy]})
    end
end