class CreateOrganizations < ActiveRecord::Migration
  def self.up
    create_table :organizations do |t|
      t.string :name,           default: "", limit: 250
      t.string :website,        default: "", limit: 500
      t.string :description,    default: "", limit: 2000
      t.string :access_code,    default: "", limit: 500

      t.timestamps
    end

    add_index :organizations, :name
  end

  def self.down
    remove_index :organizations, :name

    drop_table :organizations
  end
end
