class RegistrationsController < Devise::RegistrationsController
  def create
    @user = User.new(sign_up_params)
    if params[:user][:organization_attributes][:access_code].blank?
      # create new organization
      @access_code = "#{SecureRandom.urlsafe_base64(16)}#{Time.now.to_i}"
      @organization = Organization.create(name: params[:user][:organization_attributes][:name], access_code: @access_code)
      @user.organization_id = @organization.id
      @user.is_admin = true
    else
      # try and add someone to an organization
      @organization = Organization.where("name = ? AND access_code = ?", params[:user][:organization_attributes][:name], params[:user][:organization_attributes][:access_code])
      if @organization.empty?
        flash.now[:alert] = "No organization has been found with that name and access code."
        render :new
        return
      else
        @user.organization_id = @organization.first.id
      end
    end
    if @user.save
      flash[:success] = "Your account has been successfully created! Check your email for a confirmation link to activate your account."
      redirect_to sign_in_path
    else
      params[:email] = params[:user][:email]
      params[:organization] = params[:user][:organization_attributes][:name]
      params[:access_code] = params[:user][:organization_attributes][:access_code]
      flash.now[:error] = "Something went wrong! Please check the form for errors and try again."
      render :new
    end
  end

  def update
    @user = User.find(current_user.id)
    if @user.is_admin?
      if params[:user][:organization_attributes][:name].blank?
        params[:user][:organization_attributes][:name] = @user.organization.name
      end
      if params[:user][:organization_attributes][:access_code].blank?
        params[:user][:organization_attributes][:access_code] = @user.organization.access_code
      end
    end
    if account_update_params[:password].blank?
      account_update_params.delete("password")
      account_update_params.delete("password_confirmation")
    end
    if @user.update_without_password(account_update_params)
      flash[:success] = "Your account has been updated!"
      redirect_to edit_user_registration_path
    else
      flash.now[:error] = "Something went wrong! Please try again."
      render :edit
    end
  end

  def toggle_admin
    @user = User.find(params[:id])
    if @user.is_admin?
      @user.update_attributes(is_admin: false)
      flash[:warning] = "#{@user.display_name} has had their admin access revoked."
      redirect_to :back
    else
      @user.update_attributes(is_admin: true)
      flash[:warning] = "#{@user.display_name} has been granted admin access."
      redirect_to :back
    end
  end

  def invite_user
    @email = params[:email]
    @organization = current_user.organization.name
    @access_code = current_user.organization.access_code
    UserMailer.invite_user(current_user, @email, @organization, @access_code).deliver
    flash[:success] = "You have invited #{params[:email]} to join your organization!"
    redirect_to edit_user_registration_path
  end
end