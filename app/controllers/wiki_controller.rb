class WikiController < ApplicationController
  before_action :authenticate_user!

  layout "wiki_application"
end