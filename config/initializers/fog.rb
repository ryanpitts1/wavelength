CarrierWave.configure do |config|
  config.fog_credentials = {
    provider:                Figaro.env.fog_provider,
    aws_access_key_id:       Figaro.env.fog_aws_access_key_id,
    aws_secret_access_key:   Figaro.env.fog_aws_secret_access_key,
    region:                  Figaro.env.fog_region,
    # endpoint:              Figaro.env.fog_endpoint,
    # host:                  "s3.example.com",
  }

  config.fog_directory     = Figaro.env.fog_directory
  # config.fog_public      = false
  # config.fog_attributes  = {"Cache-Control"=>"max-age=315576000"}
  # config.root            = File.expand_path "../public", __FILE__
end