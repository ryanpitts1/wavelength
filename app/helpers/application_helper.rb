module ApplicationHelper
  def full_title(page_title)
      base_title = "Wavelength"
      if page_title.empty?
          base_title
      else
          "#{page_title} | #{base_title}".html_safe
      end
  end

  def flash_class(level)
    case level
      when "notice" then "alert alert-info"
      when "success" then "alert alert-success"
      when "warning" then "alert alert-warning"
      when "error" then "alert alert-danger"
      when "alert" then "alert alert-danger"
    end
  end

  def markdown(text)
    options = {
      filter_html:     true,
      hard_wrap:       true, 
      link_attributes: { rel: "nofollow", target: "_blank" },
      space_after_headers: true, 
      fenced_code_blocks: true
    }

    extensions = {
      autolink:           true,
      superscript:        true,
      disable_indented_code_blocks: true
    }

    renderer = Redcarpet::Render::HTML.new(options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)

    markdown.render(text).html_safe
  end
end
