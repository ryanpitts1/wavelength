Fabricator(:space) do
  organization { Fabricate(:organization) }
  name { Faker::Lorem.words(3).join(" ") }
  description { Faker::Lorem.paragraph }
  status { true }
end