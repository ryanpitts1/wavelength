Wavelength::Application.routes.draw do
  devise_for :users, controllers: { registrations: "registrations" }

  root to: "static_pages#home"
  get "dashboard" => "static_pages#dashboard"

  devise_scope :user do
    get "sign-in", to: "devise/sessions#new"
    get "sign-out", to: "devise/sessions#destroy"
    get "sign-up", to: "devise/registrations#new"
    post "users/toggle-admin", to: "registrations#toggle_admin"
    post "users/invite-user", to: "registrations#invite_user"
  end

  namespace "chat" do
    root to: "static_pages#index"
    post "upload", to: "static_pages#upload"
    post "remove_file", to: "static_pages#remove_file"
  end

  namespace "pm" do
    root to: "static_pages#dashboard"
    get "dashboard", to: "static_pages#dashboard"
  end

  namespace "wiki" do
    root to: "static_pages#dashboard"
    get "dashboard", to: "static_pages#dashboard"

    resources :spaces do
      get "archive", to: "spaces#archive"

      resources :pages do 
        get "archive", to: "pages#archive"

        resources :page_comments
      end
    end
  end
end
