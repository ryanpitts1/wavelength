class Space < ActiveRecord::Base
  belongs_to :organization
  has_many :pages, dependent: :destroy

  validates_presence_of :name
end