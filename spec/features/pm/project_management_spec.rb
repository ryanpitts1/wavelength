require "rails_helper"

feature "Project management" do
  let(:admin) { Fabricate(:admin_user) }
  let(:project) { Fabricate(:project) }

  before do
    admin.confirmed_at = Time.now
    admin.save
    login_as(admin, scope: :user)
  end

  scenario "User creates a project" do
    visit "/pm/projects/new"

    fill_in "Name",                 with: "Test Project"
    fill_in "Description",          with: "This is a test project."
    select("fibonacci", from: "Point Scale")
    click_button "Create Project"

    expect(page).to have_text("Test Project has been created!")
  end

  scenario "User receives an error if project creation was unsuccessful" do
    visit "/pm/projects/new"

    fill_in "Description", with: "This is a test project."
    click_button "Create Project"

    expect(page).to have_text("Something went wrong!")
    expect(page).to have_text("Name can't be blank")
  end

  scenario "User updates a project" do
    visit "/pm/projects/#{project.id}/edit"

    fill_in "Name", with: "#{project.name} - test"
    click_button "Update"

    expect(page).to have_text("has been updated!")
    expect(page).to have_text("#{project.name} - test")
  end

  scenario "User receives an error if project update was unsuccessful" do
    visit "/pm/projects/#{project.id}/edit"

    fill_in "Name",                 with: ""
    fill_in "Description",          with: "#{project.description} - test"
    click_button "Update"

    expect(page).to have_text("Something went wrong!")
    expect(page).to have_text("Name can't be blank")
  end

  scenario "User archives a space" do
    visit "/pm/projects/#{project.id}"

    click_on "Archive"

    expect(page).to have_text("has been archived!")
    expect(find(".available-projects")).not_to have_text(project.name)
  end
end