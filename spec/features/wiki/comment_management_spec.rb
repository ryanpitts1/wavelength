require "rails_helper"

feature "Comment management" do
  let(:admin) { Fabricate(:admin_user) }
  let(:comment) { Fabricate(:page_comment, user: admin) }

  before do
    admin.confirmed_at = Time.now
    admin.save
    login_as(admin, scope: :user)
  end

  scenario "User comments on a page" do
    visit "/wiki/spaces/#{comment.page.space.id}/pages/#{comment.page.id}"

    find(".new-comment-form #page_comment_content").set("This is a test comment.")
    click_button "Comment"

    expect(page).to have_text("Thank you for commenting on this page!")
    expect(page).to have_text("This is a test comment.")
    expect(page).to have_text("#{admin.display_name.upcase} said...")
  end

  scenario "User receives an error if saving the comment was unsuccessful" do
    visit "/wiki/spaces/#{comment.page.space.id}/pages/#{comment.page.id}"

    find(".new-comment-form #page_comment_content").set("")
    click_button "Comment"

    expect(page).to have_text(comment.page.name)
    expect(page).to have_text("Something went wrong...")
    expect(page).to have_text("Content can't be blank")
  end

  scenario "User updates on a comment on a page" do
    visit "/wiki/spaces/#{comment.page.space.id}/pages/#{comment.page.id}"

    find(".update-comment-form #page_comment_content").set("This is a test comment update.")
    click_button "Update"

    expect(page).to have_text("Your comment has been updated!")
    expect(page).to have_text("This is a test comment update.")
  end

  scenario "User replies to a comment on a page" do
    visit "/wiki/spaces/#{comment.page.space.id}/pages/#{comment.page.id}"

    find(".reply-comment-form #page_comment_content").set("This is a test comment reply.")
    click_button "Reply"

    expect(page).to have_text("Thank you for commenting on this page!")
    expect(page).to have_text("This is a test comment reply.")
    expect(page).to have_text("#{admin.display_name.upcase} said...")
  end
end