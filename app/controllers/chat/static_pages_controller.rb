class Chat::StaticPagesController < ChatController
  before_action :authenticate_user!

  def index
    gon.userId = current_user.id
    gon.organizationId = current_user.organization_id
    gon.userName = current_user.display_name
    gon.userEmail = current_user.email
    gon.isAdmin = current_user.is_admin
  end

  def upload
    file = File.open(params[:file].tempfile)
    uploader = ChatUploader.new
    uploader.store!(file)
    filepath = "https://#{Figaro.env.fog_endpoint}/#{Figaro.env.fog_directory}/#{uploader.path}"

    render json: { "filename" => "#{uploader.path.gsub!("chat/uploads/", "")}", "filepath" => "#{filepath}" }
  end

  def remove_file
    file = params[:file]
    uploader = ChatUploader.new
    @file = uploader.retrieve_from_store!(file)
    uploader.remove!

    respond_to do |format|
      format.js { render json: true }
    end
  end
end