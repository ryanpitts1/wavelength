require "rails_helper"

feature "Page management" do
  let(:admin) { Fabricate(:admin_user) }
  let(:space_page) { Fabricate(:page) }

  before do
    admin.confirmed_at = Time.now
    admin.save
    login_as(admin, scope: :user)
  end

  scenario "User creates a page" do
    visit "/wiki/spaces/#{space_page.space.id}/pages/new"

    fill_in "Name",                 with: "Test Page"
    fill_in "Description",          with: "This is a test page."
    click_button "Create Page"

    expect(page).to have_text("Test Page has been created!")
  end

  scenario "User creates a nested page" do
    visit "/wiki/spaces/#{space_page.space.id}/pages/new"

    fill_in "Name",                 with: "Test Page"
    fill_in "Description",          with: "This is a test page."
    select(space_page.name, from: "Parent Page")
    click_button "Create Page"

    expect(page).to have_text("Test Page has been created!")
    expect(find(".pages > .page")).to have_text(space_page.name)
    expect(find(".pages > .nested-pages > .page")).to have_text("Test Page")
    expect(find(".breadcrumb")).to have_text(space_page.space.name)
    expect(find(".breadcrumb")).to have_text(space_page.name)
    expect(find(".breadcrumb")).to have_text("Test Page")
  end

  scenario "User receives an error if page creation was unsuccessful" do
    visit "/wiki/spaces/#{space_page.space.id}/pages/new"

    fill_in "Description", with: "This is a test page."
    click_button "Create Page"

    expect(page).to have_text("Something went wrong!")
    expect(page).to have_text("Name can't be blank")
  end

  scenario "User updates a page" do
    visit "/wiki/spaces/#{space_page.space.id}/pages/#{space_page.id}/edit"

    fill_in "Name",                 with: "#{space_page.name} - test"
    fill_in "Description",          with: "#{space_page.description} - test"
    fill_in "Content",              with: "#{space_page.content} - test"
    click_button "Update"

    expect(page).to have_text("has been updated!")
    expect(page).to have_text("#{space_page.name} - test")
  end

  scenario "User receives an error if page update was unsuccessful" do
    visit "/wiki/spaces/#{space_page.space.id}/pages/#{space_page.id}/edit"

    fill_in "Name",                 with: ""
    fill_in "Description",          with: "#{space_page.description} - test"
    fill_in "Content",              with: "#{space_page.content} - test"
    click_button "Update"

    expect(page).to have_text("Something went wrong!")
    expect(page).to have_text("Name can't be blank")
  end

  scenario "User archives a page" do
    visit "/wiki/spaces/#{space_page.space.id}/pages/#{space_page.id}"

    click_on "Archive"

    expect(page).to have_text("has been archived!")
    expect(find(".recently-created-pages")).not_to have_text(space_page.name)
    expect(find(".recently-updated-pages")).not_to have_text(space_page.name)
  end
end