class ChatController < ApplicationController
  before_action :authenticate_user!

  layout "chat_application"
end