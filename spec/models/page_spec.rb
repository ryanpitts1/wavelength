require "rails_helper"

describe Page do
  let(:page) { Fabricate(:page) }

  it "has valid factory" do
    expect(page).to be_valid
  end

  it "is invalid when given no name" do
    page.name = nil

    expect(page).to_not be_valid
    expect(page.errors[:name]).to include("can't be blank")
  end
end