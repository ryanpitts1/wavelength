class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  layout :layout_by_resource

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected
    def layout_by_resource
      if (controller_name == "static_pages" && action_name != "dashboard") || ((controller_name == "registrations" || controller_name == "sessions" || controller_name == "passwords" || controller_name == "confirmations" || controller_name == "unlocks") && action_name == "new")
        "site_application"
      else
        "app_application"
      end
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:id, :organization_id, :email, :display_name, :password, :password_confirmation, :is_admin, {organization_attributes: [:id, :name, :website, :description, :access_code]}) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:id, :organization_id, :email, :display_name, :password, :password_confirmation, :is_admin, {organization_attributes: [:id, :name, :website, :description, :access_code]}) }
    end

    def after_sign_in_path_for(resource)
      stored_location_for(resource) || dashboard_path
    end

    def after_sign_out_path_for(resource_or_scope)
      sign_in_path
    end
end
