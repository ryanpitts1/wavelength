$(function(){
    $(".pages").find(".page .glyphicon").on("click", function(){
        $(this).parent().next("ul").toggle();
        $(this).toggleClass("glyphicon-folder-open glyphicon-folder-close");
    });

    if ($(".breadcrumb > li").last().text() == "Edit Page") {
        var parentHref = $(".breadcrumb > li").last().prev().prev().find("a").attr("href");;
    } else {
        var parentHref = $(".breadcrumb > li").last().prev().find("a").attr("href");
    }
    expandParent(parentHref);
});

var expandParent = function(parentHref) {
    $(".pages-list .pages .page a[href='" + parentHref + "']").prev(".glyphicon").trigger("click");
    if ($(".pages-list .pages .page a[href='" + parentHref + "']").parent().parent().prev(".page").length > 0) {
        parentHref = $(".pages-list .pages .page a[href='" + parentHref + "']").parent().parent().prev(".page").find("a").attr("href");
        expandParent(parentHref);
    }
}