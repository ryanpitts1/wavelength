(function($) {
    if (!$ || (parseInt($().jquery.replace(/\./g, ""), 10) < 170)) {
        throw new Error("jQuery 1.7 or later required!");
    }

    var root = this,
        previousFirechatUI = root.FirechatUI;

    root.FirechatUI = FirechatUI;

    if (!self.FirechatDefaultTemplates) {
        throw new Error("Unable to find chat templates!");
    }

    function FirechatUI(firebaseRef, el, options) {
        var self = this;

        if (!firebaseRef) {
            throw new Error("FirechatUI: Missing required argument `firebaseRef`");
        }

        if (!el) {
            throw new Error("FirechatUI: Missing required argument `el`");
        }

        options = options || {};
        this._options = options;

        this._el = el;
        this._user = null;
        this._chat = new Firechat(firebaseRef, options);

        // A list of rooms to enter once we've made room for them (once we've hit the max room limit).
        this._roomQueue = [];

        // Define some constants regarding maximum lengths, client-enforced.
        this.maxLengthUsername = 15;
        this.maxLengthUsernameDisplay = 15;
        this.maxLengthRoomName = 24;
        this.maxLengthMessage = 120;
        this.maxUserSearchResults = 100;

        // Define some useful regexes.
        this.urlPattern = /\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;
        this.pseudoUrlPattern = /(^|[^\/])(www\.[\S]+(\b|$))/gim;

        this._renderLayout();

        // Grab shortcuts to commonly used jQuery elements.
        this.$wrapper = $("#chat-container");
        this.$roomList = $("#available-chat-rooms-list");
        this.$tabList = $("#chat-room-list");
        this.$tabContent = $("#chat-windows");
        this.$messages = {};
        this.$roomUsers = {};

        // Rate limit messages from a given user with some defaults.
        this.$rateLimit = {
            limitCount: 10,         // max number of events
            limitInterval: 10000,   // max interval for above count in milliseconds
            limitWaitTime: 30000,   // wait time if a user hits the wait limit
            history: {}
        };

        // Setup UI bindings for chat controls.
        this._bindUIEvents();

        // Setup bindings to internal methods
        this._bindDataEvents();
    }

    FirechatUI.noConflict = function noConflict() {
        root.FirechatUI = previousFirechatUI;
        return FirechatUI;
    };

    FirechatUI.prototype = {
        _bindUIEvents: function() {
            // Chat-specific custom interactions and functionality.
            this._bindForHeightChange();
            this._bindForTabControls();
            this._bindForRoomList();
            this._bindForUserSearch();
            this._bindForRoomListing();

            // Generic, non-chat-specific interactive elements.
            this._setupTabs();
            // this._setupDropdowns();
            this._bindTextInputFieldLimits();
        },

        _bindDataEvents: function() {
            this._chat.on("user-update", this._onUpdateUser.bind(this));

            // Bind events for new messages, enter / leaving rooms, and user metadata.
            this._chat.on("room-enter", this._onEnterRoom.bind(this));
            this._chat.on("room-exit", this._onLeaveRoom.bind(this));
            this._chat.on("message-add", this._onNewMessage.bind(this));
            this._chat.on("message-update", this._onUpdateMessage.bind(this));
            this._chat.on("message-remove", this._onRemoveMessage.bind(this));
            this._chat.on("user-enter", this._onUserEnter.bind(this));
            this._chat.on("user-leave", this._onUserLeave.bind(this));
        },

        _renderLayout: function() {
            var template = FirechatDefaultTemplates["templates/layout-full.html"];
            $(this._el).html(template({
                maxLengthUsername: this.maxLengthUsername
            }));
        },

        _onUpdateUser: function(user) {
            // Update our current user state and render latest user name.
            this._user = user;

            // Update our interface to reflect which users are muted or not.
            var mutedUsers = this._user.muted || {};
            $("[data-event='firechat-user-mute-toggle']").each(function(i, el) {
                var userId = $(this).closest("[data-user-id]").data("user-id");
                $(this).toggleClass("red", !!mutedUsers[userId]);
            });

            // Ensure that all messages from muted users are removed.
            for (var userId in mutedUsers) {
                $(".message[data-user-id='" + userId + "']").fadeOut();
            }
        },

        _onEnterRoom: function(room) {
            this.attachTab(room.id, room.name);
        },

        _onLeaveRoom: function(roomId) {
            this.removeTab(roomId);

            // Auto-enter rooms in the queue
            if ((this._roomQueue.length > 0)) {
                this._chat.enterRoom(this._roomQueue.shift(roomId));
            }
        },

        _onNewMessage: function(roomId, message) {
            var userId = message.userId;
            if (!this._user || !this._user.muted || !this._user.muted[userId]) {
                this.showMessage(roomId, message);
            }
        },

        _onUpdateMessage: function(roomId, message) {
            var userId = message.userId;
            if (!this._user || !this._user.muted || !this._user.muted[userId]) {
                this.updateMessage(roomId, message);
            }
        },

        _onRemoveMessage: function(roomId, messageId) {
            this.removeMessage(roomId, messageId);
        },

        _onUserEnter: function(roomId, user) {
            $user = {
                id: user.id,
                name: user[Object.keys(user)[0]].name,
                email: user[Object.keys(user)[0]].email,
                organizationId: user[Object.keys(user)[0]].organizationId
            }
            $user.disableActions = (!this._user || user.id === this._user.id);
            $user.nameTrimmed = this.trimWithEllipsis(user[Object.keys(user)[0]].name, this.maxLengthUsernameDisplay);
            this.showUser(roomId, $user);
        },

        _onUserLeave: function(roomId, user) {
            this.removeUser(roomId, user.id);
        }
    };

    FirechatUI.prototype.setUser = function(userId, organizationId, userName, userEmail, isAdmin) {
        var self = this;

        // Initialize data events
        self._chat.setUser(userId, organizationId, userName, userEmail, isAdmin, function(user) {
            self._user = user;

            // if (self._chat.userIsAdmin()) {
                self._bindSuperuserUIEvents();
            // }

            self._chat.resumeSession();
        });
    };

    FirechatUI.prototype.on = function(eventType, cb) {
        var self = this;

        this._chat.on(eventType, cb);
    };

    FirechatUI.prototype._bindSuperuserUIEvents = function() {
        var self = this,
        parseMessageVars = function(event) {
            var $this = $(this),
            messageId = $this.closest("[data-message-id]").data("message-id"),
            userId = $("[data-message-id='" + messageId + "']").closest("[data-user-id]").data("user-id"),
            roomId = $("[data-message-id='" + messageId + "']").closest("[data-room-id]").data("room-id");

            return { messageId: messageId, userId: userId, roomId: roomId };
        };

        // Handle click of the 'Delete Message' icon.
        $(document).delegate("[data-event='message-delete']", "click", function(event) {
            if (confirm("Are you sure you want to remove this file? This file will be gone forever!")) {
                var messageVars = parseMessageVars.call(this, event);

                $.ajax({
                    url: "/chat/remove_file",
                    type: "POST",
                    data: { file: $(this).attr("data-filename") }
                }).done(function(data) {
                    self._chat.deleteMessage(messageVars.roomId, messageVars.messageId);
                }).fail(function(data) {
                    alert("An error occurred while trying to remove your file.  Please try again or contact Wavelength support.");
                });
            }
        });
    };

    FirechatUI.prototype._bindForHeightChange = function() {
        var self = this,
        $el = $(this._el),
        lastHeight = null;

        setInterval(function() {
            var height = $el.height();
            if (height != lastHeight) {
                lastHeight = height;
                $(".chat").each(function(i, el) {
                });
            }
        }, 500);
    };

    FirechatUI.prototype._bindForTabControls = function() {
        var self = this;

        // Handle click of tab close button.
        $(document).delegate("[data-event='firechat-close-tab']", "click", function(event) {
            var roomId = $(this).closest("[data-room-id]").data("room-id");
            self._chat.leaveRoom(roomId);
            return false;
        });
    };

    FirechatUI.prototype._bindForRoomList = function() {
        var self = this;

        $("#chat-btn-rooms").bind("click", function(event) {
            event.preventDefault();

            if (!$(".available-chat-rooms").hasClass("hidden")) {
                return;
            } else {
                $(".available-chat-rooms").removeClass("hidden");
            }

            var $this = $(this),
            template = FirechatDefaultTemplates["templates/room-list-item.html"],
            selectRoomListItem = function() {
                var parent = $(this).closest("li"),
                roomId = parent.data("room-id"),
                roomName = parent.data("room-name");

                if (self.$messages[roomId]) {
                    self.focusTab(roomId);
                } else {
                    self._chat.enterRoom(roomId, roomName);
                }

                return false;
            };

            leaveRoomListItem = function() {
                var parent = $(this).closest("li"),
                roomId = parent.data("room-id"),
                roomName = parent.data("room-name");

                if (self.$messages[roomId]) {
                    self._chat.leaveRoom(roomId);
                }

                return false;
            };

            self._chat.getRoomList(function(rooms) {
                self.$roomList.empty();
                for (var roomId in rooms) {
                    var room = rooms[roomId];
                    if (room.type != "public") continue;
                    room.isRoomOpen = !!self.$messages[room.id];
                    var $roomItem = $(template(room));
                    $roomItem.children("a.enter-room").bind("click", selectRoomListItem);
                    $roomItem.children("a.leave-room").bind("click", leaveRoomListItem);
                    self.$roomList.append($roomItem.toggle(true));
                }
            });
        });

        $("#close-available-chat-rooms").bind("click", function(event) {
            event.preventDefault();

            if (!$(".available-chat-rooms").hasClass("hidden")) {
                $(".available-chat-rooms").addClass("hidden");
            }
        });
    };

    FirechatUI.prototype._bindForUserSearch = function() {
        var self = this,
        handleUserSearchSubmit = function(event) {
            var $this = $(this),
            targetId = $this.data("target"),
            controlsId = $this.data("controls"),
            templateId = $this.data("template"),
            prefix = $this.val() || $this.data("prefix") || "",
            startAt = $this.data("startAt") || null,
            endAt = $this.data("endAt") || null;

            event.preventDefault();

            userSearch(targetId, templateId, controlsId, prefix, startAt, endAt);
        },
        userSearch = function(targetId, templateId, controlsId, prefix, startAt, endAt) {
            var $target = $("#" + targetId),
            $controls = $("#" + controlsId),
            template = FirechatDefaultTemplates[templateId];

            // Query results, filtered by prefix, using the defined startAt and endAt markets.
            self._chat.getUsersByPrefix(prefix, startAt, endAt, self.maxUserSearchResults, function(users) {
                var numResults = 0,
                $prevBtn, $nextBtn, username, firstResult, lastResult;

                $target.empty();

                for (username in users) {
                    var user = users[username];

                    // Disable buttons for <me>.
                    user.disableActions = (!self._user || user.id === self._user.id);

                    numResults += 1;

                    $target.append(template(user));

                    // If we've hit our result limit, the additional value signifies we should paginate.
                    if (numResults === 1) {
                        firstResult = user.name.toLowerCase();
                    } else if (numResults >= self.maxUserSearchResults) {
                        lastResult = user.name.toLowerCase();
                        break;
                    }
                }

                if ($controls) {
                    $prevBtn = $controls.find("[data-toggle='firechat-pagination-prev']");
                    $nextBtn = $controls.find("[data-toggle='firechat-pagination-next']");

                    // Sort out configuration for the "next" button
                    if (lastResult) {
                        $nextBtn
                        .data("event", "firechat-user-search")
                        .data("startAt", lastResult)
                        .data("prefix", prefix)
                        .removeClass("disabled").removeAttr("disabled");
                    } else {
                        $nextBtn
                        .data("event", null)
                        .data("startAt", null)
                        .data("prefix", null)
                        .addClass("disabled").attr("disabled", "disabled");
                    }
                }
            });
        };

        $(document).delegate("[data-event='firechat-user-search']", "keyup", handleUserSearchSubmit);
        $(document).delegate("[data-event='firechat-user-search']", "click", handleUserSearchSubmit);

        // Upon click of the dropdown, autofocus the input field and trigger list population.
        $(document).delegate("[data-event='firechat-user-search-btn']", "click", function(event) {
            event.stopPropagation();
            var $input = $(this).next("div.firechat-dropdown-menu").find("input");
            $input.focus();
            $input.trigger(jQuery.Event("keyup"));
        });

        // Ensure that the dropdown stays open despite clicking on the input element.
        $(document).delegate("[data-event='firechat-user-search']", "click", function(event) {
            event.stopPropagation();
        });
    };

    FirechatUI.prototype._bindForRoomListing = function() {
        var self = this,
            $createRoomButton = $(".new-room-link"),
            $createRoomField = $("#chat-input-room-name");

        $createRoomButton.on("click", function(event){
            event.preventDefault();

            $(".new-room .glyphicon").toggleClass("glyphicon-chevron-up glyphicon-chevron-down");
            $(".new-room-form").slideToggle();
        });

        // Handle click of the create new room button.
        $createRoomField.bind("keydown", function(event) {
            if (event.which === 13) {
                var name = $createRoomField.val();
                if (name !== "") {
                    self._chat.createRoom(name, "public");
                    $(".new-room-link").trigger("click");
                    $(this).val("");
                    return false;
                }
            }
        });
    };

    FirechatUI.prototype._setupTabs = function() {
        var self = this,
        show = function($el) {
            var $this = $el,
            $ul = $this.closest("ul"),
            selector = $this.attr("data-target"),
            previous = $ul.find(".active:last a")[0],
            $target,
            event;

            if (!selector) {
                selector = $this.attr("href");
                selector = selector && selector.replace(/.*(?=#[^\s]*$)/, "");
            }

            if ($this.parent("li").hasClass("active")) return;

            event = $.Event("show", { relatedTarget: previous });

            $this.trigger(event);

            if (event.isDefaultPrevented()) return;

            $target = $(selector);

            activate($this.parent("li"), $ul);
            activate($target, $target.parent(), function () {
                $this.trigger({
                    type: "shown",
                    relatedTarget: previous
                });
            });
        },
        activate = function (element, container, callback) {
            var $active = container.find("> .active"),
            transition = callback && $.support.transition && $active.hasClass("fade");

            function next() {
                $active.removeClass("active");

                element.addClass("active");

                if (transition) {
                    element.addClass("in");
                } else {
                    element.removeClass("fade");
                }

                if (callback) {
                callback();
                }
            }

            if (transition) {
                $active.one($.support.transition.end, next);
            } else {
                next();
            }

            $active.removeClass("in");
        };

        $(document).delegate("[data-toggle='chat-tab']", "click", function(event) {
            event.preventDefault();

            if (!$(".available-chat-rooms").hasClass("hidden")) {
                $(".available-chat-rooms").addClass("hidden");
            }

            show($(this));
        });
    };

    FirechatUI.prototype._bindTextInputFieldLimits = function() {
        $("body").delegate("input[data-provide='limit'], textarea[data-provide='limit']", "keyup", function(event) {
            var $this = $(this),
            $target = $($this.data("counter")),
            limit = $this.attr("maxlength"),
            count = $this.val().length;

            $target.html(Math.max(0, limit - count));
        });
    };

    FirechatUI.prototype.attachTab = function(roomId, roomName) {
        var self = this;

        // If this tab already exists, give it focus.
        if (self.$messages[roomId]) {
            self.focusTab(roomId);
            return;
        }

        var room = {
            id: roomId,
            name: roomName
        };

        // Populate and render the tab content template.
        var tabTemplate = FirechatDefaultTemplates["templates/tab-content.html"],
            $tabContent = $(tabTemplate(room));
        self.$tabContent.append($tabContent);
        
        var $messages = $("#chat-messages" + roomId),
            $roomUsers = $("#chat-room-user-list-" + roomId);

        // Keep a reference to the message listing for later use.
        self.$messages[roomId] = $messages;

        // Keep a reference to the room user list for later use.
        self.$roomUsers[roomId] = $roomUsers;

        // Attach keyup and on-enter events to textarea.
        var $textarea = $tabContent.find("textarea").last(),
            $newMessagePanel = $textarea.closest(".new-message-panel"),
            $messageForm = $newMessagePanel.find("form"),
            $uploadBtn = $textarea.prev(".upload-btn"),
            $fileUpload = $uploadBtn.prev(".file-upload");

        $textarea.bind("keyup", function(event) {
            var $message = $textarea.val(),
                $message = $message.replace(/\s*$/,"");

            // send message if enter/return was hit and move the message list relative to the new message panel height
            if ((event.which === 13) && $message !== "" && !event.shiftKey) {
                event.preventDefault();

                self._chat.sendMessage(roomId, $message);
                $textarea.val("");
                $(this).height(parseFloat($(this).css("min-height")) != 0 ? parseFloat($(this).css("min-height")) : parseFloat($(this).css("font-size")));
                $messages.css("bottom", $newMessagePanel.outerHeight());

                return false;
            } else {
                // check to see if backspace or delete was pressed, if so, it resets the height of the box so it can be resized properly
                if (event.which == 8 || event.which == 46) {
                    $(this).height(parseFloat($(this).css("min-height")) != 0 ? parseFloat($(this).css("min-height")) : parseFloat($(this).css("font-size")));
                }

                // the following will help the text expand as typing takes place
                while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                    $(this).height($(this).height()+1);
                };

                // move the message list relative to the new message panel height
                $messages.css("bottom", $newMessagePanel.outerHeight());
                $messages.scrollTop($messages[0].scrollHeight);
            }
        });

        // bind the click event for the file upload
        $uploadBtn.bind("click", function() {
            $fileUpload.trigger("click");
        });
        $fileUpload.bind("change", function(event) {
            var formData = new FormData();
            formData.append("file", $fileUpload[0].files[0]);

            $.ajax({
                url: "/chat/upload",
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json"
            }).done(function(data) {
                self._chat.sendMessage(roomId, data, "image");
            }).fail(function(data) {
                alert("An error occurred while trying to upload your file.  Please try again or contact Wavelength support.");
            });

            event.preventDefault();
        });

        // Populate and render the tab menu template.
        var tabListTemplate = FirechatDefaultTemplates["templates/tab-menu-item.html"];
        var $tab = $(tabListTemplate(room));
        self.$tabList.append($tab);

        // Attach on-shown event to move tab to front and scroll to bottom.
        $tab.bind("shown", function(event) {
            $messages.scrollTop($messages[0].scrollHeight);
        });

        // Update the room listing to reflect that we're now in the room.
        self.$roomList.children("[data-room-id=" + roomId + "]").children("a").addClass("highlight");

        // Automatically select the new tab.
        self.focusTab(roomId);
    };

    FirechatUI.prototype.removeTab = function(roomId) {
        delete this.$messages[roomId];

        // Remove the inner tab content.
        this.$tabContent.find("[data-room-id=" + roomId + "]").remove();

        // Remove the tab from the navigation menu.
        this.$tabList.find("[data-room-id=" + roomId + "]").remove();

        // Automatically select the next tab if there is one.
        this.$tabList.find("[data-toggle='chat-tab']").last().trigger("click");

        // Update the room listing to reflect that we're now in the room.
        this.$roomList.children("[data-room-id=" + roomId + "]").children("a").removeClass("highlight");
    };

    FirechatUI.prototype.focusTab = function(roomId) {
        var self = this;

        if (self.$messages[roomId]) {
            var $tabLink = self.$tabList.find("[data-room-id='" + roomId + "']").find("a");
            if ($tabLink.length) {
                $tabLink.last().trigger("click");
                var $this = $(".chat-window.active"),
                    roomId = $this.data("room-id"),
                    targetId = "chat-room-user-list-"+roomId,
                    $target = $("#" + targetId);

                $target.empty();
                self._chat.getUsersByRoom(roomId, function(users) {
                    for (var username in users) {
                        user = users[username];
                        user.disableActions = (!self._user || user.id === self._user.id);
                        user.nameTrimmed = self.trimWithEllipsis(user.name, self.maxLengthUsernameDisplay);
                        self.showUser(roomId, user);
                    }
                    self.sortListLexicographically("#" + targetId);
                });

                if (!$(".available-chat-rooms").hasClass("hidden")) {
                    $(".available-chat-rooms").addClass("hidden");
                }
            }
        }
    };

    FirechatUI.prototype.showMessage = function(roomId, rawMessage) {
        var self = this;

        // Setup defaults
        var message = {
            id              : rawMessage.id,
            localtime       : self.formatTime(rawMessage.timestamp),
            editedTime      : ((rawMessage.updateTimestamp) ? self.formatTime(rawMessage.updateTimestamp) : ""),
            message         : rawMessage.message || "",
            filename        : rawMessage.filename || "",
            userId          : rawMessage.userId,
            name            : rawMessage.name,
            type            : rawMessage.type || "default",
            isSelfMessage   : (self._user && rawMessage.userId == self._user.id),
            disableActions  : (!self._user || rawMessage.userId == self._user.id)
        };

        // While other data is escaped in the Underscore.js templates, escape and
        // process the message content here to add additional functionality (add links).
        // Also trim the message length to some client-defined maximum.
        // message.message = _.map(message.message.split(" "), function(token) {
        //     if (self.urlPattern.test(token) || self.pseudoUrlPattern.test(token)) {
        //         return self.linkify(encodeURI(token));
        //     } else {
        //         return _.escape(token);
        //     }
        // }).join(" ");

        // Populate and render the message template.
        var template = FirechatDefaultTemplates["templates/message.html"];
        var $message = $(template(message));
        var $messages = self.$messages[roomId];
        if ($messages) {
            var scrollToBottom = false;
            if ($messages.scrollTop() / ($messages[0].scrollHeight - $messages[0].offsetHeight) >= 0.95) {
                // Pinned to bottom
                scrollToBottom = true;
            } else if ($messages[0].scrollHeight <= $messages.height()) {
                // Haven't added the scrollbar yet
                scrollToBottom = true;
            }

            $messages.append($message);

            var $newMessage = $messages.find($message),
                messageId = $newMessage.attr("data-message-id"),
                $messageMenu = $newMessage.find(".message-menu"),
                $renderedMessage = $newMessage.find(".message-content > .message-rendered"),
                $rawMessage = $newMessage.find(".message-content > .message-raw"),
                $rawMessageTextarea = $rawMessage.find("textarea");


            // Attach an event to view the textarea to update a message
            $newMessage.find(".message-menu > .message-edit").on("click", function() {
                if ($rawMessage.hasClass("hide")) {
                    var renderedHeight = $renderedMessage.height(),
                        rawMessage = $rawMessageTextarea.val();

                    $rawMessage.height(renderedHeight);
                    $rawMessageTextarea.height(renderedHeight);
                    $renderedMessage.toggleClass("hide");
                    $rawMessage.toggleClass("hide");
                    $rawMessageTextarea.focus().val("").val(rawMessage);
                } else {
                    $rawMessage.toggleClass("hide");
                    $renderedMessage.toggleClass("hide");
                }
                $messageMenu.find(".message-edit").toggleClass("glyphicon-pencil glyphicon-remove");
            });

            // Attach an event to send the updated message
            $rawMessageTextarea.bind("keyup", function(event) {
                var $message = $rawMessageTextarea.val(),
                    $message = $message.replace(/\s*$/,"");

                // update message if enter/return was hit and move the message list relative to the new message content height
                if (event.which === 13 && !event.shiftKey) {
                    event.preventDefault();

                    $rawMessageTextarea.val("");
                    self._chat.updateMessage(roomId, messageId, $message);
                    $rawMessageTextarea.val($message);

                    $rawMessage.toggleClass("hide");
                    $renderedMessage.toggleClass("hide");
                    $messageMenu.find(".message-edit").toggleClass("glyphicon-pencil glyphicon-remove");
                    $(this).height(parseFloat($(this).css("min-height")) != 0 ? parseFloat($(this).css("min-height")) : parseFloat($(this).css("font-size")));

                    scrollToBottom = false;

                    return false;
                } else {
                    // check to see if backspace or delete was pressed, if so, it resets the height of the box so it can be resized properly
                    if (event.which == 8 || event.which == 46) {
                        $(this).height(parseFloat($(this).css("min-height")) != 0 ? parseFloat($(this).css("min-height")) : parseFloat($(this).css("font-size")));
                    }

                    // the following will help the text expand as typing takes place
                    while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                        $(this).height($(this).height()+1);
                        $(this).closest(".message-raw").height($(this).height()+1);
                    };
                }
            });

            if (scrollToBottom) {
                $messages.scrollTop($messages[0].scrollHeight);
            }
        }
    };

    FirechatUI.prototype.updateMessage = function(roomId, rawMessage) {
        var self = this;

        // Setup defaults
        var message = {
            id              : rawMessage.id,
            localtime       : self.formatTime(rawMessage.timestamp),
            editedTime      : ((rawMessage.updateTimestamp) ? self.formatTime(rawMessage.updateTimestamp) : false),
            message         : rawMessage.message || "",
            userId          : rawMessage.userId,
            name            : rawMessage.name,
            type            : rawMessage.type || "default",
            isSelfMessage   : (self._user && rawMessage.userId == self._user.id),
            disableActions  : (!self._user || rawMessage.userId == self._user.id)
        };

        if (message.editedTime) {
            if (message.message == "") {
                $("li[data-message-id='" + message.id + "']").find(".message-content > .message-rendered .message-rendered-content").html("");
                $("li[data-message-id='" + message.id + "']").find(".message-content > .message-rendered > .messge-update-timestamp").html(message.name + " deleted message at " + message.editedTime);
            } else {
                $("li[data-message-id='" + message.id + "']").find(".message-content > .message-rendered .message-rendered-content").html(marked(message.message, { breaks: true }));
                $("li[data-message-id='" + message.id + "']").find(".message-content > .message-rendered > .messge-update-timestamp").html(" - Edited at " + message.editedTime);
            }
        }
    };

    FirechatUI.prototype.removeMessage = function(roomId, messageId) {
        $(".chat-message[data-message-id='" + messageId + "']").remove();
    };

    FirechatUI.prototype.showUser = function(roomId, user) {
        if ($("#chat-room-user-list-" + roomId).find("li[data-user-id='" + user.id + "']").length == 0) {
            var template = FirechatDefaultTemplates["templates/room-user-list-item.html"],
                targetId = "chat-room-user-list-"+roomId,
                $target = $("#" + targetId);

            $target.append($(template(user)));

            $(".online-users-" + roomId).text($("#chat-room-user-list-" + roomId + " li").length);
        }
    };

    FirechatUI.prototype.removeUser = function(roomId, userId) {
        $("#chat-room-user-list-" + roomId).find("li[data-user-id='" + userId + "']").remove();
    };

    FirechatUI.prototype.sortListLexicographically = function(selector) {
        $(selector).children("li").sort(function(a, b) {
            var upA = $(a).text().toUpperCase();
            var upB = $(b).text().toUpperCase();
            return (upA < upB) ? -1 : (upA > upB) ? 1 : 0;
        }).appendTo(selector);
    };

    FirechatUI.prototype.trimWithEllipsis = function(str, length) {
        str = str.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
        return (length && str.length <= length) ? str : str.substring(0, length) + "...";
    };

    FirechatUI.prototype.formatTime = function(timestamp) {
        var date = (timestamp) ? new Date(timestamp) : new Date(),
        hours = date.getHours() || 12,
        minutes = "" + date.getMinutes(),
        ampm = (date.getHours() >= 12) ? "pm" : "am";

        hours = (hours > 12) ? hours - 12 : hours;
        minutes = (minutes.length < 2) ? "0" + minutes : minutes;
        return "" + hours + ":" + minutes + ampm;
    };
})(jQuery);