require "rails_helper"

feature "Space management" do
  let(:admin) { Fabricate(:admin_user) }
  let(:space) { Fabricate(:space) }

  before do
    admin.confirmed_at = Time.now
    admin.save
    login_as(admin, scope: :user)
  end

  scenario "User creates a space" do
    visit "/wiki/spaces/new"

    fill_in "Name",                 with: "Test Space"
    fill_in "Description",          with: "This is a test space."
    click_button "Create Space"

    expect(page).to have_text("Test Space has been created!")
  end

  scenario "User receives an error if space creation was unsuccessful" do
    visit "/wiki/spaces/new"

    fill_in "Description", with: "This is a test space."
    click_button "Create Space"

    expect(page).to have_text("Something went wrong!")
    expect(page).to have_text("Name can't be blank")
  end

  scenario "User updates a space" do
    visit "/wiki/spaces/#{space.id}/edit"

    fill_in "Name", with: "#{space.name} - test"
    click_button "Update"

    expect(page).to have_text("has been updated!")
    expect(page).to have_text("#{space.name} - test")
  end

  scenario "User receives an error if space update was unsuccessful" do
    visit "/wiki/spaces/#{space.id}/edit"

    fill_in "Name",                 with: ""
    fill_in "Description",          with: "#{space.description} - test"
    click_button "Update"

    expect(page).to have_text("Something went wrong!")
    expect(page).to have_text("Name can't be blank")
  end

  scenario "User archives a space" do
    visit "/wiki/spaces/#{space.id}"

    click_on "Archive"

    expect(page).to have_text("has been archived!")
    expect(find(".available-spaces")).not_to have_text(space.name)
  end
end