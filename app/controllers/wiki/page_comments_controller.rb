class Wiki::PageCommentsController < WikiController
  before_action :authenticate_user!

  def create
    @page_comment = PageComment.new(page_comment_params)
    @page_comment.page_id = params[:page_id]
    @page_comment.user_id = current_user.id
    if @page_comment.save!
      flash[:success] = "Thank you for commenting on this page!"
      redirect_to :back
    else
      flash[:error] = "Something went wrong! Please try again."
      redirect_to :back
    end
  end

  def update
    @page_comment = PageComment.find(params[:id])
    if @page_comment.update!(page_comment_params)
      flash[:success] = "Your comment has been updated!"
      redirect_to :back
    else
      flash[:error] = "Something went wrong! Please try again."
      redirect_to :back
    end
  end

  def destroy
    @page_comment = PageComment.find(params[:id])
    @page_comment.destroy
    flash[:warning] = "Your comment and all replies (if any) hav been deleted."
    redirect_to :back
  end

  private
    def page_comment_params
      params.require(:page_comment).permit(:page_id, :user_id, :parent_id, :content, :_destroy)
    end
end