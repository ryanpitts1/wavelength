require "rails_helper"

feature "Admin deletes user" do
  let(:admin_user) { Fabricate(:admin_user) }
  let(:user) { Fabricate(:user) }

  before do
    admin_user.confirmed_at = Time.now
    admin_user.save

    user.confirmed_at = Time.now
    user.organization_id = admin_user.organization.id
    user.save

    login_as(admin_user, scope: :user)
    visit "/users/edit"
  end

  scenario "Users count equals 2" do
    expect(admin_user.organization.users.count).to eq(2)
  end

  scenario "Admin user can delete another user" do
    click_on "Delete?"

    expect(admin_user.organization.users.count).to eq(1)
  end
end