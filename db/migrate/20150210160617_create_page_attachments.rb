class CreatePageAttachments < ActiveRecord::Migration
  def self.up
    create_table :page_attachments do |t|
      t.integer :page_id,           null: false
      t.string :name,               default: "", limit: 500
      t.string :file,               default: "", limit: 500
      t.string :aws_path,           default: "", limit: 500

      t.timestamps
    end

    add_index :page_attachments, :file
  end

  def self.down
    remove_index :page_attachments, :file

    drop_table :page_attachments
  end
end
