class PmController < ApplicationController
  before_action :authenticate_user!

  layout "pm_application"
end