require "rails_helper"

feature "User sign out" do
  before do
    user = Fabricate(:user)
    user.confirmed_at = Time.now
    user.save
    login_as(user, scope: :user)
  end

  scenario "User successfully signs out" do
    visit "/sign-out"

    expect(page).to have_content("Signed out successfully.")
  end
end