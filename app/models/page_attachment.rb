class PageAttachment < ActiveRecord::Base
  mount_uploader :file, PageAttachmentUploader
  before_save :set_aws_path

  belongs_to :page

  validates_presence_of :name
  validates_presence_of :file

  def filename 
    read_attribute(:file) 
  end

  private
    def set_aws_path
      self.aws_path = "#{self.aws_path}/#{self.page.id}"
    end
end