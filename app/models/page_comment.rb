class PageComment < ActiveRecord::Base
  has_ancestry

  belongs_to :page
  belongs_to :user

  validates_presence_of :content
end
