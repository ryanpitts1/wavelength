class Wiki::PagesController < WikiController
  before_action :authenticate_user!

  def show
    @space = Space.find(params[:space_id])
    @page = Page.find(params[:id])
    @page_comment = PageComment.new

    add_breadcrumb @space.name, wiki_space_path(@space)
    @page.ancestors.each do |a|
      add_breadcrumb a.name, wiki_space_page_path(@space, a)
    end
    add_breadcrumb @page.name, wiki_space_page_path(@space, @page)
  end

  def new
    @space = Space.find(params[:space_id])
    @pages_for_select = ancestry_options(Page.arrange(space_id: params[:space_id], order: "sort_order ASC, name ASC")) { |i| "#{"-" * i.depth} #{i.name}" }
    @page = Page.new
  end

  def create
    @space = Space.find(params[:space_id])
    @page = Page.new(page_params)
    if @page.save
      flash[:success] = "#{@page.name} has been created!"
      redirect_to wiki_space_page_path(params[:space_id], @page.id)
    else
      @pages_for_select = ancestry_options(Page.arrange(space_id: params[:space_id], order: "sort_order ASC, name ASC")) { |i| "#{"-" * i.depth} #{i.name}" }
      flash.now[:error] = "Something went wrong! Please try again."
      render :new
    end
  end

  def edit
    @space = Space.find(params[:space_id])
    @pages_for_select = ancestry_options(Page.arrange(space_id: params[:space_id], order: "sort_order ASC, name ASC")) { |i| "#{"-" * i.depth} #{i.name}" }
    @page = Page.find(params[:id])

    add_breadcrumb @space.name, wiki_space_path(@space)
    @page.ancestors.each do |a|
      add_breadcrumb a.name, wiki_space_page_path(@space, a)
    end
    add_breadcrumb @page.name, wiki_space_page_path(@space, @page)
    add_breadcrumb "Edit Page", edit_wiki_space_page_path(@space, @page)
  end

  def update
    @page = Page.find(params[:id])
    if @page.update(page_params)
      flash[:notice] = "#{@page.name} has been updated!"
      redirect_to wiki_space_page_path(params[:space_id], params[:id])
    else
      @space = Space.find(params[:space_id])
      @pages_for_select = ancestry_options(Page.arrange(space_id: params[:space_id], order: "sort_order ASC, name ASC")) { |i| "#{"-" * i.depth} #{i.name}" }
      flash.now[:error] = "Something went wrong! Please try again."
      render :edit
    end
  end

  def destroy
    @space = Space.find(params[:space_id])
    @page = Page.find(params[:id])
    @page.destroy
    flash[:warning] = "#{@page.name} has been deleted."
    redirect_to wiki_space_path(@space)
  end

  def archive
    @page = Page.find(params[:page_id])
    @page.status = false
    if @page.save
      flash[:success] = "#{@page.name} has been archived!"
      redirect_to wiki_space_path(params[:space_id])
    else
      flash[:error] = "Something went wrong! Please try again."
      redirect_to :back
    end
  end

  def add_page_comment
    @space = Space.find(params[:space_id])
    @page = Page.find(params[:page_id])
    @page_comment = PageComment.new(page_params)
    if @page_comment.save
      flash[:success] = "Thank you for commenting on this page!"
      redirect_to :back
    else
      @pages_for_select = ancestry_options(Page.arrange(space_id: params[:space_id], order: "sort_order ASC, name ASC")) { |i| "#{"-" * i.depth} #{i.name}" }

      add_breadcrumb @space.name, wiki_space_path(@space)
      @page.ancestors.each do |a|
        add_breadcrumb a.name, wiki_space_page_path(@space, a)
      end
      add_breadcrumb @page.name, wiki_space_page_path(@space, @page)

      flash.now[:error] = "Something went wrong! Please try again."
      render :new
    end
  end

  private
    def page_params
      params.require(:page).permit(:space_id, :user_id, :parent_id, :name, :description, :content, :sort_oder, :status, :_destroy, {user_attributes: [:id, :organization_id, :email, :display_name, :is_admin]}, {page_attachments_attributes: [:id, :page_id, :name, :file, :aws_path, :remove_file, :_destroy]}, {page_comments_attributes: [:id, :page_id, :user_id, :parent_id, :content, :_destroy]})
    end

    def ancestry_options(items, &block)
      return ancestry_options(items){ |i| "#{"-" * i.depth} #{i.name}" } unless block_given?

      result = []
      items.map do |item, sub_items|
        result << [yield(item), item.id]
        result += ancestry_options(sub_items, &block)
      end
      result
    end
end