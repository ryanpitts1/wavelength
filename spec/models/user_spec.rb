require "rails_helper"

describe User do
  let(:user) { Fabricate(:user) }

  it "has valid factory" do
    expect(user).to be_valid
  end

  it "is invalid when given no display name" do
    user.display_name = nil

    expect(user).to_not be_valid
    expect(user.errors[:display_name]).to include("can't be blank")
  end

  it "is invalid when given no email" do
    user.email = nil

    expect(user).to_not be_valid
    expect(user.errors[:email]).to include("can't be blank")
  end

  it "is invalid when given an invalid email" do
    user.email = "test@example"

    expect(user).to_not be_valid
  end
end